# coding: utf-8
from requests_html import HTMLSession
import chardet
from unidecode import unidecode


session = HTMLSession()
url = 'https://www.marxists.org/archive/marx/works/subject/quotes/index.htm'

r = session.get(url)
r.encoding = 'UTF-8'
r.html.render(sleep=1)
paragraphs = r.html.find('p')
quotes = []
for item in paragraphs:
    if item.text != "&nbsp;":
        quotes.append(item.text)
quotes = list(filter(None, quotes))
del quotes[-1]
del quotes[-1]
del quotes[1]
del quotes[0]