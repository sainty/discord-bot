import discord 
import os
import asyncio
from discord.ext import commands
from discord.utils import get

intents = discord.Intents(messages=True, guilds=True, reactions=True, members=True, presences=True)
client = commands.Bot(command_prefix = '>', intents=intents)

#validates user request for protected commands
def is_it_me(ctx):
    return ctx.author.id==169329481201352704

#######    Client Events   ########
@client.event
async def on_ready():
    print('Giuseppe is Active')

#join message
@client.event
async def on_member_join(member):
    await client.get_channel(776334738348179479).send(f'Look {member}, joined the server what a wise guy.')
    print(f'{member} joined')

#remove message
@client.event
async def on_member_remove(member):
    await client.get_channel(776334738348179479).send(f'Get outta here {member}')
    print(f'{member} left')

# error handling
@client.event
async def on_command_error(ctx, error):
    #checks if unauthorised user tries accessing protected commands
    if isinstance(error, commands.CheckFailure):
        await ctx.send('DO NOT FUCK WITH MY BOT!')
    else:
        print(error)

#######    Cog Commands   ####### 

#loads cog
@client.command()
@commands.check(is_it_me)
async def load(ctx, extension):
    print(f'load {extension}')
    client.load_extension(f'cogs.{extension}')

#unloads cog
@client.command()
@commands.check(is_it_me)
async def unload(ctx, extension):
    print(f'unload {extension}')
    client.unload_extension(f'cogs.{extension}')

#reloads cog
@client.command()
@commands.check(is_it_me)
async def reload(ctx, extension):
    print(f'reload {extension}')
    client.reload_extension(f'cogs.{extension}')

if __name__ == "__main__":
    #loads all the cogs
    for file in os.listdir('./cogs'):
        if file.endswith('.py'):
            client.load_extension(f'cogs.{file[:-3]}')

    client.run('Nzc2MzI0NTExNDA2MzU4NTI4.X6zOig.Jo1mlUqtfC_Ze0bk3CWqvjz9eNE')

    