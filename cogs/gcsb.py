import discord
import random
from discord.ext import commands
from Lists import n_words, gcsb_words, banned_words, n_word_responses

class GCSB(commands.Cog):
    def __init__(self, client):
        self._client = client

    #sends a discord message in response to keywords
    @commands.Cog.listener()
    async def on_message(self, message):
        #prevents message loop
        if message.author == self._client:
            return

        #asks for a response from message_handler
        response = self.message_handler(message)

        #checks if key word is empty if not messages the response.
        if response:
            await message.channel.send(response)
    
    #Checks for special words returns none if none found
    def message_handler(self, message):
        #searchs for words in gcsb_words from list.py
        for i in gcsb_words:
            if i in message.content.lower().strip():
                print('gcsb response triggered')
                return 'https://cdn.discordapp.com/attachments/645490848162643989/939713630096740513/lepolicehasarrived.mp4'
        
        #searchs for words in banned_words from list.py
        for j in banned_words:
            if j in message.content.lower().strip():
                print('banned word triggered')
                return "woah woah woah we don't do that anymore."
                
        #searchs for words in n_words from list.py
        for k in n_words:
            if k in message.content.lower().strip():
                print('n word triggered')
                return random.choice(n_word_responses)

        return None
                


def setup(client):
    client.add_cog(GCSB(client))
