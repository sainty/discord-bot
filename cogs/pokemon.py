import os, sys
sys.path.insert(1, os.path.join(sys.path[0], '..'))
from Lists import pokemon_names

import random
import discord
from discord.ext import commands

class Pokemon(commands.Cog):
    def __init__(self, client):
        self._client = client

    @commands.command(aliases=['oscarsucks'], help='Creates two randomised Pokemon teams.')
    async def pokemonteams(self, ctx):
        await ctx.send(f'Team 1:\n{self.generate_team()} \nTeam 2:\n{self.generate_team()}')
    
    def generate_team(self):
        print('Pokemon Team')
        team = ''
        for _ in range(6):
            team += f'\t{self.random_pokemon()}\n'
        return team

    def random_pokemon(self):
        return random.choice(pokemon_names)

def setup(client):
    client.add_cog(Pokemon(client))