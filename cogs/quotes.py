import discord
import random
from discord.ext import commands

import os, sys
sys.path.insert(1, os.path.join(sys.path[0], '..'))
from Lists import marx_quotes, giuseppe_quotes, member_q

class Quotes(commands.Cog):
    def __init__(self, client):
        self._client = client

    #sends 'cope harder' translated to a random language
    @commands.command(aliases=['pwncuckservative', 'marx'], help='Libertarian Propaganda.')
    async def marx_quote(self, ctx):
        print('Marx')
        await ctx.send(random.choice(marx_quotes))
    
    #amusing league quote
    @commands.command(aliases=['league?', 'weed'])
    async def leaguetime(self, ctx):
        print('League Time')
        await ctx.send("3.. 2.. 1.. Deploying tactical smokes.")
    
    #amusing italian quote
    @commands.command(aliases=['canyoubelievethisguy', 'mamamia'])
    async def getaloadofthisguy(self, ctx):
        print('Mamamia')
        await ctx.send(random.choice(giuseppe_quotes))

    #gcsb protection device
    @commands.command(aliases=['commitaxfraud', 'bombthebehive', 'arson', 'murder', ], help='Legal defense.')
    async def gcsb(self, ctx):
        print('gcsb')
        await ctx.send("In minecraft :)")
    
    #information about discord members
    @commands.command(aliases=['who',], help='Information about discord members.')
    async def who_is(self, ctx, user):
        print(f"who is {user}")
        user = user.lower()
        await ctx.send(f"{user} is:\n{random.choice(member_q[user])}")

    #best ideology list
    @commands.command(aliases=['ideology'])
    async def leaguetime(self, ctx):
        print('best ideology')
        await ctx.send("https://cdn.discordapp.com/attachments/645490848162643989/956450357221924904/cAAAAASUVORK5CYII.png")

def setup(client):
    client.add_cog(Quotes(client))
