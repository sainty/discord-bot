import discord
import random
from discord.ext import commands
from googletrans import Translator, LANGCODES

class Cope(commands.Cog):
    def __init__(self, client):
        self._client = client
        self._translator = Translator()

    #sends 'cope harder' translated to a random language
    @commands.command(aliases=['rebuttal', 'cope'], help='Defeat any opponent with an intellectual reply.')
    async def intellecutal_riposte(self, ctx):
        print('Cope')
        await ctx.send(self.translation())
    
    def translation(self):
        random_country = random.choice(list(LANGCODES.values()))
        return self._translator.translate('cope harder', 
                                        dest=random_country).text
        
def setup(client):
    client.add_cog(Cope(client))